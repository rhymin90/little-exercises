package group.msg;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlSplitter {

  private static final String PATTERN_STRING = ""; // TODO define pattern

  /** Extract the following */
  public static List<String> split(String url) {

    Pattern extractPattern = Pattern.compile(PATTERN_STRING);
    Matcher matcher = extractPattern.matcher(url);

    List<String> groups = new ArrayList<>();
    if (matcher.matches()) {

      for (int i = 0; i < matcher.groupCount(); i++) {
        groups.add(matcher.group(i));
      }
    }
    return groups;
  }
}
