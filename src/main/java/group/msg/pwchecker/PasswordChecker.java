package group.msg.pwchecker;

import java.util.function.Predicate;

public class PasswordChecker {

  /**
   * Password checking function that should check if the given password <br>
   * - is not empty <br>
   * - has a length of at least 8 characters <br>
   * - contains at least one number <br>
   * - contains at least one lowercase letter <br>
   * - contains at least one special character
   *
   * @param password - the given password
   * @return if password satisfies the needed conditions
   */
  public static boolean check(String password) {
    boolean valid = false;

    valid =
        valid
            && hasAtLeastOneCharacterSatisfyingCondition(
                password,
                asciiCharacter -> {
                  return asciiCharacter > 0;
                });

    return valid;
  }

  public static boolean hasAtLeastOneCharacterSatisfyingCondition(
      String password, Predicate<Character> condition) {
    char[] allCharacters = password.toCharArray();
    for (char c : allCharacters) {
      if (condition.test(c)) {
        return true;
      }
    }
    return false;
  }
}
