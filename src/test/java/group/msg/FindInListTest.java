package group.msg;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class FindInListTest {

  @Order(1)
  @Test
  void findMaxValue() {
    // given
    List<Integer> numbers = List.of(1, 3, 5, 7, 9, 11, 13);

    // when
    int max = FindInList.findMaxValue(numbers);

    // then
    assertEquals(13, max, "Max value in " + numbers + " should be 13");
  }

  @Order(2)
  @Test
  void findAnotherMaxValue() {
    // given
    List<Integer> numbers = List.of(77, 99, 33, 44);

    // when
    int max = FindInList.findMaxValue(numbers);

    // then
    assertEquals(99, max, "Max value in " + numbers + " should be 99");
  }

  @Order(3)
  @Test
  void findMinValue() {
    // given
    List<Integer> numbers = List.of(1, 3, 5, 7, 9, 11, 13);

    // when
    int min = FindInList.findMinValue(numbers);

    // then
    assertEquals(1, min, "Min value in " + numbers + " should be 1");
  }

  @Order(4)
  @Test
  void findAnotherMinValue() {
    // given
    List<Integer> numbers = List.of(55, 88, 11, 99);

    // when
    int min = FindInList.findMinValue(numbers);

    // then
    assertEquals(1, min, "Min value in " + numbers + " should be 11");
  }
}
