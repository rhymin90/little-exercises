package group.msg;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UrlSplitterTest {

  @Test
  void split() {

    // given
    String url = "https://google.com/my-site";

    // when
    List<String> groups = UrlSplitter.split(url);

    // then
    var expectedList = List.of("https://", "google.com", "my-site");

    assertEquals(expectedList.size(), groups.size(), "Group list should be of size 3");
    assertEquals(
        expectedList.get(0), groups.get(0), "First group element should be " + groups.get(0));
    assertEquals(
        expectedList.get(1), groups.get(1), "Second group element should be " + groups.get(1));
    assertEquals(
        expectedList.get(2), groups.get(2), "Third group element should be " + groups.get(2));
  }
}
