package group.msg.calculator;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CalculatorTest {

  @Order(1)
  @ParameterizedTest(name = "{0} plus {1} = {2}")
  @CsvSource({
    "0,    1,   1",
    "1,    2,   3",
    "49,  51, 100",
    "1,  100, 101",
    "-1,  100, 99",
  })
  void add(int first, int second, int expectedResult) {
    Calculator calculator = new Calculator();
    assertEquals(
        expectedResult,
        calculator.add(first, second),
        () -> first + " plus " + second + " should equal " + expectedResult);
  }

  @Order(2)
  @ParameterizedTest(name = "{0} minus {1} = {2}")
  @CsvSource({
    "1,    1,   0",
    "3,    2,   1",
    "100,  49,  51",
    "-101, -100, -1",
  })
  void subtract(int first, int second, int expectedResult) {
    Calculator calculator = new Calculator();
    assertEquals(
        expectedResult,
        calculator.subtract(first, second),
        () -> first + " minus " + second + " should equal " + expectedResult);
  }

  @Order(3)
  @ParameterizedTest(name = "{0} multiplied by {1} = {2}")
  @CsvSource({
    "0,    1,   0",
    "1,    2,   2",
    "4,    5,   20",
    "1,  100, 100",
    "-1,  100, -100",
  })
  void multiply(int first, int second, int expectedResult) {
    Calculator calculator = new Calculator();
    assertEquals(
        expectedResult,
        calculator.multiply(first, second),
        () -> first + " multiplied by " + second + " should equal " + expectedResult);
  }

  @Order(4)
  @ParameterizedTest(name = "{0} divided by {1} = {2}")
  @CsvSource({
    "0,    1,   0",
    "1,    2,   0",
    "9,    3,   3",
    "1,  100,   0",
    "10,  10,   1",
    "10,  -10,  -1",
    "10,  0,    0",
  })
  void divide(int first, int second, int expectedResult) {
    Calculator calculator = new Calculator();
    assertEquals(
        expectedResult,
        calculator.divide(first, second),
        () -> first + " divided by " + second + " should equal " + expectedResult);
  }

  @Order(5)
  @ParameterizedTest(name = "{0} divided by {1} = {2}")
  @CsvSource({
    "0,    1,   0.0",
    "1,    2,   0.5",
    "9,    3,   3",
    "1,  100,   0.01",
    "10,  10,   1",
    "10,  -10,  -1",
    "10,  0,    0",
  })
  void divideWithDecimal(int first, int second, double expectedResult) {
    Calculator calculator = new Calculator();
    assertEquals(
        expectedResult,
        calculator.divideWithDecimal(first, second),
        () -> first + " divided by " + second + " should equal " + expectedResult);
  }

  @Order(6)
  @ParameterizedTest(name = "{0} modulo {1} = {2}")
  @CsvSource({
    "2,    1,   0",
    "9,    3,   0",
    "10,   3,   1",
    "9,  5,   4",
    "10,  9,   1",
    "100,  30,    10",
  })
  void modulo(int first, int second, int expectedResult) {
    Calculator calculator = new Calculator();
    assertEquals(
        expectedResult,
        calculator.modulo(first, second),
        () -> first + " modulo " + second + " should equal " + expectedResult);
  }

  @Order(7)
  @ParameterizedTest(name = "{0} nth fibonacci = {2}")
  @CsvSource({
    "1,  1",
    "2,  1",
    "3,  2",
    "4,  3",
    "5,  5",
    "6,  8",
    "7,  13",
    "8,  21",
    "9,  34,",
    "10, 55,",
    "33, 3524578",
    "46, 1836311903",
    "47, 0", // fallback as number too big
  })
  void fibonacci(int n, int expectedResult) {
    Calculator calculator = new Calculator();
    assertEquals(
        expectedResult, calculator.fib(n), () -> n + " nth fibonacci should be " + expectedResult);
  }
}
