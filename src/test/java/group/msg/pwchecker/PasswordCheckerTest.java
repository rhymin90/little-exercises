package group.msg.pwchecker;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PasswordCheckerTest {

  @Order(1)
  @Test
  void checkPasswordEmptyNotValid() {
    // given
    final var password = "";
    // when
    var valid = PasswordChecker.check(password);
    //
    assertEquals(false, valid, "Password '" + password + "' should not be valid");
  }

  @Order(2)
  @ParameterizedTest(name = "password \"{0}\" is at least 8 characters long? {1}")
  @CsvSource({
    "start, false",
    "start123, true",
    "staaaaaart, true",
    "staaaaaaaaaaaart, true",
  })
  void checkPasswordLongEnough(String password, boolean valid) {
    // when
    var result = PasswordChecker.check(password);
    //
    assertEquals(
        valid, result, "Password '" + password + "' should be " + (valid ? "valid" : "invalid"));
  }

  @Order(3)
  @ParameterizedTest(name = "password \"{0}\" has at least one number in it? {1}")
  @CsvSource({
    "startstart, false",
    "start123, true",
    "123start, true",
    "st4rtEnd, true",
  })
  void checkPasswordHasNumber(String password, boolean valid) {
    // when
    var result = PasswordChecker.check(password);
    //
    assertEquals(
        valid, result, "Password '" + password + "' should be " + (valid ? "valid" : "invalid"));
  }

  @Order(4)
  @ParameterizedTest(name = "password \"{0}\" has at least one lowercase letter in it? {1}")
  @CsvSource({
    "STARTSTART, false",
    "123456, false",
    "start123, true",
  })
  void checkPasswordHasLowercaseLetter(String password, boolean valid) {
    // when
    var result = PasswordChecker.check(password);
    //
    assertEquals(
        valid, result, "Password '" + password + "' should be " + (valid ? "valid" : "invalid"));
  }

  @Order(5)
  @ParameterizedTest(name = "password \"{0}\" has at least one uppercase letter in it? {1}")
  @CsvSource({
    "startstart, false",
    "123456, false",
    "startStart, true",
  })
  void checkPasswordHasUppercaseLetter(String password, boolean valid) {
    // when
    var result = PasswordChecker.check(password);
    //
    assertEquals(
        valid, result, "Password '" + password + "' should be " + (valid ? "valid" : "invalid"));
  }

  @Order(6)
  @ParameterizedTest(name = "password \"{0}\" has at least one special character in it? {1}")
  @CsvSource({
    "startstart, false",
    "123456, false",
    "startStart, false",
    "start!, true",
  })
  void checkPasswordHasSpecialCharacter(String password, boolean valid) {
    // when
    var result = PasswordChecker.check(password);
    //
    assertEquals(
        valid, result, "Password '" + password + "' should be " + (valid ? "valid" : "invalid"));
  }
}
